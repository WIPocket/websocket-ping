import * as WebSocket from 'ws';
const WebSocketServer = WebSocket.default.Server;

import { createServer } from 'http';
import { createReadStream } from 'fs';

const server = createServer((req, res) => {
  createReadStream("index.html").pipe(res);
});
const wss = new WebSocketServer({ server });

wss.on('connection', (ws) => {
  ws.on('message', (data) => {
    ws.send(data.toString());
    console.log(data.toString());
  });
});

server.listen(8080);
console.log("Listening http://localhost:8080/");
